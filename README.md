# Meme Generator
Welcome to Meme Generator! Meme Generator allows a user to choose a meme template, fill in the meme with text, and then download the meme. Extra features have been implemented for this project, such as manually resizing the text and moving the text on the meme images.

Want to make your own memes? Clone this repo, cd into the correct directory, and run `npm i && npm start` to get started!




