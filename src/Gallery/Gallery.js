import React, { Component } from "react";
import Image from "../Image/Image";
import "./ImageList.css"
import Nav from "../Nav/Nav";

export default class Gallery extends Component {
  render() {
    return (
      <div>
        <Nav/>
        <div className="image-list">
          {this.props.images.map((image) => {
            return (
              <Image
                key={image.id}
                image={image}
                handleclick={this.props.handleclick}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
